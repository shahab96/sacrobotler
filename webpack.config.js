/* eslint-disable */

var webpack = require('webpack');
var path = require('path');
var fs = require('fs');

var nodeModules = {};
fs.readdirSync('node_modules')
  .filter(x => ['.bin'].indexOf(x) === -1)
  .forEach((mod) => {
    nodeModules[mod] = `commonjs ${mod}`;
  });

module.exports = {
  entry: './build/bot.js',
  mode: 'production',
  target: 'node',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bot.js',
  },
  externals: nodeModules,
};
