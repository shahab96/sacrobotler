import botver from '../commands/bot-ver';
import allow from '../commands/allow';
import revoke from '../commands/revoke';

const commandFunctions = {
  '!botver': botver,
  '!allow': allow,
  '!revoke': revoke,
};

export default async ({
  message,
  bot,
  command,
  params,
}) => {
  commandFunctions[command]({
    message,
    bot,
    params,
  });
};
