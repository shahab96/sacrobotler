import mongoose from 'mongoose';

const guild = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  Id: String,
  Channels: [{
    Id: String,
    Name: String,
    AllowedCommands: [String],
  }],
  RoleOverrides: [{
    Author: String,
    AllowedCommands: [String],
  }],
});

export default mongoose.model('Guild', guild);
