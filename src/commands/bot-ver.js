import { version } from '../../package.json';
import errorHandler from '../error-handler';

// Just return the current package.json version.
export default ({
  message,
  bot,
}) => {
  try {
    message.channel.send(`I am currently version ${version}!`);
  } catch (err) {
    errorHandler(message, err, bot);
  }
};
