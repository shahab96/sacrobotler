import Guild from '../models/guild';
import errorHandler from '../error-handler';
import commandList from '../constants/commands.json';

export default async ({
  message,
  bot,
  params,
}) => {
  try {
    const [command, target] = params;
    if (!commandList.includes(command)) return;
    const { guild } = message;
    const guildInfo = await Guild.find({
      Id: guild.id,
    }).exec();
    const roleOverride = guildInfo[0].RoleOverrides.find(ro => ro.Author === target.substr(2, 18));
    if (!roleOverride || !roleOverride.AllowedCommands.includes(command)) {
      message.channel.send(`${target} does not have override access to ${command}`);
      return;
    }
    if (roleOverride) {
      roleOverride.AllowedCommands = roleOverride.AllowedCommands.filter(cmd => cmd !== command);
      await guildInfo[0].set({
        RoleOverrides: guildInfo[0].RoleOverrides,
      }).save();
      message.channel.send(`Override access to ${command} revoked for ${target}`);
    }
  } catch (err) {
    errorHandler(message, err, bot);
  }
};
