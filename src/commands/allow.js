import Guild from '../models/guild';
import errorHandler from '../error-handler';
import commandList from '../constants/commands.json';

export default async ({
  message,
  bot,
  params,
}) => {
  try {
    const [command, target] = params;
    if (!commandList.includes(command)) return;
    const { guild } = message;
    const guildInfo = await Guild.find({
      Id: guild.id,
    }).exec();
    const roleOverride = guildInfo[0].RoleOverrides.find(ro => ro.Author === target.substr(2, 18));
    if (roleOverride) {
      if (roleOverride.AllowedCommands.includes(command)) {
        message.channel.send(`${target} already has override access to ${command}`);
        return;
      }
      roleOverride.AllowedCommands.push(command);
    } else {
      guildInfo[0].RoleOverrides.push({
        Author: message.author.id,
        AllowedCommands: [command],
      });
    }
    await guildInfo[0].set({
      RoleOverrides: guildInfo[0].RoleOverrides,
    }).save();
    message.channel.send(`${target} now has override access to ${command}`);
  } catch (err) {
    errorHandler(message, err, bot);
  }
};
