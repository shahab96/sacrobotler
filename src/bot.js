import { Client } from 'discord.js';
import mongoose, { Types } from 'mongoose';
import Guild from './models/guild';
import errorHandler from './error-handler';
import executeCommand from './events/on-message';
import commandList from './constants/commands.json';

const bot = new Client();

mongoose.connect(process.env.MONGO_CONNECTION_STRING, {
  useNewUrlParser: true,
});

// Once the bot has logged in
bot.on('ready', async () => {
  const link = await bot.generateInvite([
    'SEND_MESSAGES',
    'VIEW_CHANNEL',
    'MANAGE_MESSAGES', // This is to delete the message people send to vote to keep their votes secret.
  ]);
  console.log(link); // eslint-disable-line no-console
});

// When joining a guild
bot.on('guildCreate', async (guild) => {
  const {
    channels,
    id,
    owner,
  } = guild;
  await new Guild({
    _id: new Types.ObjectId(),
    Id: id,
    Channels: channels.map(ch => ({
      Id: ch.id,
      Name: ch.name,
      AllowedCommands: [],
    })),
    RoleOverrides: [{
      Author: owner.id,
      AllowedCommands: commandList,
    }],
  }).save();
});

// When leaving a guild
bot.on('guildDelete', async (guild) => {
  await Guild.deleteOne({
    Id: guild.id,
  }).exec();
});

// Triggered for every message
bot.on('message', async (message) => {
  const {
    guild,
    channel,
    author,
    content,
  } = message;
  try {
    if (author.bot) return;
    if (channel.type === 'dm') return;
    if (!content.startsWith('!')) return;

    const guildInfo = await Guild.find({ Id: guild.id }).exec();
    const [command, ...params] = content.split(' ');
    const { Channels, RoleOverrides } = guildInfo[0];
    const sourceChannel = Channels.find(ch => ch.Id === channel.id);
    const roleOverride = RoleOverrides.find(ro => ro.Author === author.id);

    const messageData = {
      message,
      command,
      params,
      bot,
    };

    if (!sourceChannel) return;

    const { AllowedCommands } = sourceChannel;
    if (commandList.includes(command)) {
      if (AllowedCommands.includes(command)) {
        await executeCommand(messageData);
      } else if (roleOverride) {
        const overriddenCommands = roleOverride.AllowedCommands;
        if (overriddenCommands.includes(command)) await executeCommand(messageData);
      }
    }
  } catch (err) {
    errorHandler(message, err, bot);
  }
});

bot.login(process.env.TOKEN);
