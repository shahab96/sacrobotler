import { RichEmbed } from 'discord.js';

export default (message, error, bot) => {
  const embed = new RichEmbed()
    .setAuthor('Botler Error Logger')
    .setDescription(error.stack)
    .addField('Triggered By', message.author.username)
    .addField('Input', message.content);

  bot.channels.get(process.env.ERROR_LOG_CHANNEL).send({ embed });
};
