#!/bin/bash

set -e
cd ~/sacrobotler
git fetch
git checkout master
git pull
npm install
npm start
