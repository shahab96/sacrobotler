#!/bin/bash

set -e
eval $(ssh-agent -s)
chmod a+x ./scripts/disableHostKeyChecking.sh
chmod a+x ./scripts/updateAndRestart.sh
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
./scripts/disableHostKeyChecking.sh
echo "Deploying to server."
ssh ec2-user@$DEPLOY_SERVER 'bash' < ./scripts/updateAndRestart.sh
